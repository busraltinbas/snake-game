﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple_Script : MonoBehaviour
{

    public TMPro.TextMeshProUGUI skor_Txt;
    int score = 0;

    Snake_Move kuyruk_Olustur;

    private void Start()
    {
        kuyruk_Olustur = GameObject.Find("Snake_Head").GetComponent<Snake_Move>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Snake_Head")
        {

            score += 10;
            skor_Txt.text = "SCORE" + score;

            koordinat_Degistir();
            
        }

        if (col.gameObject.tag == "Queue")
        {


            koordinat_Degistir();
            kuyruk_Olustur.kuyruk_arttir();
            
          
        }
    }


    void koordinat_Degistir()
    {
        float x_deger = Random.Range(-2.0f, 0.9f);
        float z_deger = Random.Range(14.0f, -4.0f);

        transform.position = new Vector3(x_deger, 1, z_deger);
    }
}   