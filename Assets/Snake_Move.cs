﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Snake_Move : MonoBehaviour
{

    public GameObject kuyruk;
    List<GameObject> kuyruklar;


    Vector3 eski_koordinat;
    GameObject eski_kuyruk;
    float hiz = 45.0f;

    public GameObject GameOver_Pnl;

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Tas")
        {
            GameOver_Pnl.SetActive(true);
            Time.timeScale = 0.0f;

        }

        if (col.gameObject.tag == "Wall")
        {
            GameOver_Pnl.SetActive(true);
            Time.timeScale = 0.0f;
                    
        }
    }

public void Tekrar_oyna()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Scenes/Oyun");    
    
    }


    void Start()
    {

        kuyruklar = new List<GameObject>();
        for (int i = 0; i < 5; i++)
        {

            GameObject yeni_kuyruk = Instantiate(kuyruk, transform.position, Quaternion.identity);
            kuyruklar.Add(yeni_kuyruk);


        }

        InvokeRepeating("hareket_et", 0, 0.1f);

    }

    void hareket_et()
    {

        eski_koordinat = transform.position;

        transform.Translate(0, 0, hiz * Time.deltaTime);

        kuyruk_Takip();

    }


    public void kuyruk_arttir()
    {
        GameObject yeni_kuyruk = Instantiate(kuyruk, transform.position, Quaternion.identity);
        kuyruklar.Add(yeni_kuyruk);
    }


    void kuyruk_Takip()
    {


        kuyruklar[0].transform.position = eski_koordinat;
        eski_kuyruk = kuyruklar[0];
        kuyruklar.RemoveAt(0);
        kuyruklar.Add(eski_kuyruk);


    }

    public void Rotate(float aci)
    {
        transform.Rotate(0, aci, 0);
    }



    // Update is called once per frame
    void Update()
    {

    }
}